# -*- coding: utf-8 -*-
# Author; alin m elena, alin@elena.re
# Contribs;
# Date: 23-11-2023
# ©alin m elena, GPL v3 https://www.gnu.org/licenses/gpl-3.0.en.html

from __future__ import annotations

import numpy as np

from ase import Atoms
from ase.io.trajectory import Trajectory
from ase.eos import EquationOfState
from ase.io import read, write
from ase.units import kJ
from ase import io
from ase.filters import ExpCellFilter,FrechetCellFilter
from ase.optimize import LBFGS
from mlip_calculators import choose_calculator


import argparse
import pathlib
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.core import Structure

cli=argparse.ArgumentParser()
cli.add_argument(
  "--device",
  type=str,
  default='cuda',
  choices = ['cuda','cpu','mps'],
  help = ' provide device to run. default %(default)s'
)

cli.add_argument(
  "--precision",
  type=str,
  default='float64',
  choices = ['float64','float32'],
  help = ' provide precision to run. default %(default)s'
)

cli.add_argument(
  "--d3",
  action="store_true",
  help = 'enable d3? default %(default)s'
)

cli.add_argument(
  "--system",
  type=str,
  default=None,
  help = 'system coordinates to simulate, ase readable, default %(default)s'
)

cli.add_argument(
  "--min_scale",
  type=float,
  default=0.94,
  help = 'minimum scale in 0-1, default %(default)s'
)

cli.add_argument(
  "--max_scale",
  type=float,
  default=1.06,
  help = 'max scale in 1-, default %(default)s'
)

cli.add_argument(
  "--images",
  type=int,
  default=7,
  help = 'minimum scale in integers, default %(default)s'
  )
cli.add_argument(
  "--model",
  type=str,
  default='',
  help = 'provide the mace model file. default: %(default)s'
)

cli.add_argument(
  "--minimize_fmax",
  type=float,
  default=0.01,
  help = 'provide minimizer precision for forces in eV/A, default %(default)s'
  )

def optimize(a: Atoms ,fmax : float ,optf: str) -> None:
  ua = FrechetCellFilter(a, hydrostatic_strain=True, constant_volume = True)
  dyn = LBFGS(ua)
  dyn.run(fmax=fmax,steps=1000)

args = cli.parse_args()

model = args.model
device = args.device
system = args.system
precision = args.precision
d3 = args.d3
n = args.images
smin = args.min_scale
smax = args.max_scale
fmax = args.minimize_fmax
try:
  sys = read(system,format="cif")
except:
  s = Structure.from_file(system, primitive=False)
  sys = AseAtomsAdaptor.get_atoms(s)

sysname = pathlib.Path(system).stem
arch = "mace"
if model in ['small', 'medium', 'large']:
  arch = "mace_mp"
  calculator =choose_calculator(architecture=arch, model=model, dispersion=d3, default_dtype=precision, device=device)
elif model in ['small-off', 'medium-off', 'large-off']:
  arch = "mace_off"
  calculator =choose_calculator(architecture=arch, model=model.split("-")[0], dispersion=d3, default_dtype=precision, device=device)
else:
  calculator =choose_calculator(architecture=arch, model_paths=model, dispersion=d3, default_dtype=precision, device=device)

sys.calc = calculator

m = model
if d3:
    m = f"{model}-d3"

try:
  optimize(sys,fmax,f"{sysname}-opt.xyz")

except ValueError:
  print(sysname, 'Failed GO not supported elements')
  raise
except Exception:
  print(f"{sysname} failed GO too many steps ")
  raise

cell = sys.get_cell()

g = np.linspace(smin, smax, n)**(1/3)
volumes = []
energies = []
print(len(g))
for i,x in enumerate(g):
    sys.set_cell(cell * x, scale_atoms=True)
    optimize(sys,fmax,f"{sysname}-{x:.03f}-opt.xyz")
    energies.append(sys.get_potential_energy())
    volumes.append(sys.get_volume())
    write(f"{sysname}-{m}-{i:02d}.cif",sys)

ff = open(f"{sysname}-{m}-raw.dat","w")
for d1,d2,d3 in zip(g,energies,volumes):
	print(f"{d1} {d2} {d3}",file=ff)
ff.close()
eos = EquationOfState(volumes, energies,'birchmurnaghan')
v0, e0, B = eos.fit()
print(sysname, B / kJ * 1.0e24, 'GPa')
eos.plot(f'{sysname}-{m}-eos.png')

