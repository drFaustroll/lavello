from __future__ import annotations

import phonopy as phonons
from ase import Atoms
from phonopy.phonon.band_structure import get_band_qpoints_and_path_connections, get_band_qpoints_by_seekpath
from phonopy.file_IO import write_force_constants_to_hdf5
from phonopy.structure.atoms import PhonopyAtoms
from phonopy.phonon.dos import get_pdos_indices
from ase.io import read, write

import matplotlib.pyplot as plt

import datetime as clock
from pathlib import Path
import os
import argparse
from janus_core.calculations.geom_opt import GeomOpt
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import spglib
import yaml as yml
from ase.spacegroup.symmetrize import refine_symmetry
from copy import copy
from numpy import min, max

def getminmax(a: list ,b : list ,ymin: float ,ymax: float) -> (float,float):
    inds = [ i for i,x in enumerate(a) if x >= ymin and x<= ymax]
    s = [inds[0],inds[-1]]
    xmin = min(b[inds[0]:inds[-1]+1])
    xmax = max(b[inds[0]:inds[-1]+1])
    return (xmin,xmax)

def plot_band_structure_dos_and_pdos(ph,ymin=None,ymax=None):
  pdi = get_pdos_indices(ph.primitive_symmetry)
  legend = ['Total']+ [ph.primitive.symbols[x[0]] for x in pdi]
  n = len([x for x in ph._band_structure.path_connections if not x]) + 1
  fig = plt.figure()
  axs = ImageGrid(
      fig,
      111,  # similar to subplot(111)
      nrows_ncols=(1, n),
      axes_pad=0.11,
      label_mode="L",
  )
  if ymin is None:
    ph._band_structure.plot(axs[:-1])
  else:
    np = ph.band_structure.path_connections.count(False)
    bs=ph.band_structure.plot(axs[:-1])

    # _ = [ axs[i].set_ylim(ymin,ymax) for i in range(np) ]
    # _ = [ axs[i].get_yaxis().set_visible(False) for i in range(np) if i > 0  ]
  ph._total_dos.plot(axs[-1], xlabel="", ylabel="", draw_grid=False, flip_xy=True  )
  try:
    ph._pdos.plot(axs[-1],
          indices=pdi,
          xlabel="",
          ylabel="",
          legend = legend,
          draw_grid=False,
          flip_xy=True,
      )
  except:
      pass
  if ymin is None:
    xlim = axs[-1].get_ylim()
    ylim = axs[-1].get_xlim()
  else:
    xlim = [ymin,ymax]
    ylim = getminmax(ph._total_dos._frequency_points, ph._total_dos._dos,ymin,ymax)
  aspect = (ylim[1] - ylim[0]) / (xlim[1] - xlim[0]) * 3
  axs[-1].set_aspect(aspect)
  axs[-1].axhline(y=0, linestyle=":", linewidth=0.5, color="b")
  axs[-1].set_ylim((xlim[0], xlim[1]))
  axs[-1].set_xlim((ylim[0], ylim[1]))
  return plt

cli=argparse.ArgumentParser()
cli.add_argument(
  "--d3",
  action="store_true",
  help = 'enable d3? default %(default)s'
)

cli.add_argument(
  "--dos",
  action="store_true",
  help = 'enable dos/pdos calcualtion? default %(default)s'
)

cli.add_argument(
  "--symmetrize",
  action="store_true",
  help = 'enable symmetrization of structure and force constants? default %(default)s'
)

cli.add_argument(
  "--thermal",
  action="store_true",
  help = 'enable thermal? default %(default)s'
)

cli.add_argument(
  "--opt_cell",
  action="store_true",
  help = 'optimize cell in addition to coordinates  default %(default)s'
)

cli.add_argument(
  "--model",
  type=str,
  default='small',
  help = 'provide the mace model file. default: %(default)s'
)

cli.add_argument(
  "--arch",
  type=str,
  default='mace_mp',
  help = 'provide the architecture. default: %(default)s'
)


cli.add_argument(
  "--precision",
  type=str,
  default='float64',
  choices = ['float64','float32'],
  help = ' provide precision to run. default %(default)s'
  )

cli.add_argument(
  "--system",
  type=str,
  default="Si.cif",
  help = 'system coordinates to simulate, ase readable, default %(default)s'
  )

cli.add_argument(
  "--minimize_fmax",
  type=float,
  default=0.0001,
  help = 'provide minimizer precision for forces in eV/A, default %(default)s'
  )

cli.add_argument(
  "--displacement",
  type=float,
  default=0.001,
  help = 'provide displacement in A for force constants calculation, default %(default)s'
  )

cli.add_argument(
  "--supercell",
  nargs=3,
  type=int,
  default=[2, 2, 2],
  help = 'provides size of supercell for calculation, default %(default)s'
  )

cli.add_argument(
  "--device",
  type=str,
  default='cpu',
  choices = ['cuda','cpu','mps'],
  help = ' provide device to run. default %(default)s'
)

cli.add_argument(
  "--paths",
  type=str,
  default = None,
  help = "provides info to generate a path of qpoints for band structure, default uses auto band"
)

cli.add_argument(
  "--t_min",
  type=float,
  default=0,
  help = 'start temperature for CV calculations, default %(default)s'
  )

cli.add_argument(
  "--t_max",
  type=float,
  default=1000,
  help = 'end temperature for CV calculations, default %(default)s'
  )

cli.add_argument(
  "--t_step",
  type=float,
  default=50,
  help = 'temperature step for CV calculations, default %(default)s'
  )

cli.add_argument(
  "--y_min",
  type=float,
  default=None,
  help = 'min y for band structure, default %(default)s'
  )

cli.add_argument(
  "--y_max",
  type=float,
  default=None,
  help = 'max y for band structure, default %(default)s'
  )

cli.add_argument(
  "--mesh",
  nargs=3,
  type=int,
  default=[10, 10, 10],
  help = 'max y for band structure, default %(default)s'
  )

args = cli.parse_args()
precision = args.precision
device = args.device
model = args.model
d3 = args.d3
dos = args.dos
fmax = args.minimize_fmax
name = args.system
ad = args.displacement
nc = args.supercell
t_min = args.t_min
t_step = args.t_step
t_max = args.t_max
ymax = args.y_max
ymin = args.y_min
m = args.mesh
arch = args.arch
paths = args.paths
symmetrize = args.symmetrize
thermal = args.thermal
sysname = Path(name).stem
opt_cell = args.opt_cell

#no magnetic moments considered
def Phonopy2ASEAtoms(s: PhonopyAtoms ) -> Atoms:

    return Atoms(symbols=s.symbols,scaled_positions=s.scaled_positions,cell=s.cell,masses=s.masses,pbc=True)

def ASE2PhonopyAtoms(s: Atoms ) -> PhonopyAtoms:

    return PhonopyAtoms(symbols=s.get_chemical_symbols(), cell=s.cell.array,
                        scaled_positions=s.get_scaled_positions(),
                        masses=s.get_masses())

def calc_forces(calculator: Calculator, s: PhonopyAtoms) -> ArrayLike:
    atoms = Phonopy2ASEAtoms(s)
    atoms.calc = copy(calculator)
    return atoms.get_forces()

result ={}

system =read(name)

supercell_matrix=((nc[0], 0, 0), (0, nc[1], 0), (0, 0, nc[2]))

ml_mod = model
model = Path(model).stem

if d3:
  model = f"{model}-d3"

model = f"{model}-{arch}"

t = clock.datetime.now()
print(f"optimisation started at {t}")
if opt_cell:
  o_sys = GeomOpt(struct=system, arch=arch, device=device,calc_kwargs={'model_paths':ml_mod,'default_dtype':precision, 'dispersion':d3}, write_results=True, fmax=fmax, write_kwargs={'filename':f"{sysname}-{model}-opt.extxyz"})
else:
  o_sys = GeomOpt(struct=system, arch=arch, device=device,calc_kwargs={'model_paths':ml_mod,'default_dtype':precision, 'dispersion':d3}, write_results=True, fmax=fmax, write_kwargs={'filename':f"{sysname}-{model}-opt.extxyz"},filter_func=None)

o_sys.run()

dt = clock.datetime.now() - t
print(f"phonons optimization finished after {dt}")

if symmetrize:
    print(f"phonons symmetrize system, after optimisation")
    x = refine_symmetry(system, symprec=1.0e-4, verbose=False)

cell = ASE2PhonopyAtoms(system)
t = clock.datetime.now()
print(f"phonons displacements started at {t}")
phonon = phonons.Phonopy(cell, supercell_matrix)
phonon.generate_displacements(distance=ad)
disp_supercells = phonon.supercells_with_displacements
dt = clock.datetime.now() - t
print(f"phonons displacements finished after {dt}")
t = clock.datetime.now()
print(f"phonons forces started at {t}")
phonon.forces = [ calc_forces(system.calc, supercell) for supercell in disp_supercells if supercell is not None ]
dt = clock.datetime.now() - t
print(f"phonons forces finished after {dt}")
t = clock.datetime.now()
print(f"phonons force constants started at {t}")
phonon.produce_force_constants()
if symmetrize:
   phonon.symmetrize_force_constants(level=1)
write_force_constants_to_hdf5(phonon.force_constants,f"{sysname}-{model}-force_constants.hdf5")
dt = clock.datetime.now() - t
print(f"phonons force constants finished after {dt}")

result["phonon"] = phonon
t = clock.datetime.now()
print(f"phonons save started at {t}")
result['phonon'].save(f"{sysname}-{model}-phonopy.yaml",settings={'force_constants': True})
dt = clock.datetime.now() - t
print(f"phonons save finished after {dt}")


t = clock.datetime.now()
print(f"phonons band structure started at {t}")
if paths is not None:
    suf = ""
    print(f"phonons use info from {paths} for band structure")
    fp = open(paths,"r+",encoding='utf-8')

    qs = yml.safe_load(fp)

    nq = sum([len(q) for q in qs['paths']])
    nl = len(qs['labels'])

    if nl != nq:
       print(f"phonons number of labels. {nl} different from number of points {nq}")

    qps, conns = get_band_qpoints_and_path_connections(band_paths=qs['paths'], npoints=qs['npoints'])
    labels = qs['labels']
else:
    print(f"phonons use auto  band structure")
    suf="-auto"
    prim = result['phonon'].primitive
    qps, labels, conns = get_band_qpoints_by_seekpath(prim,51)
#    result['phonon'].auto_band_structure(write_yaml=False)
result['phonon'].run_band_structure(paths=qps, path_connections=conns, labels=labels)

result['phonon'].write_yaml_band_structure(filename=f"{sysname}-{model}-bands.yml.xz",compression="lzma")
result['phonon'].write_hdf5_band_structure(filename=f"{sysname}-{model}-bands.hdf5")
bplt = result['phonon'].plot_band_structure()
bplt.savefig(f"{sysname}-{model}-bands{suf}.svg")

np = result['phonon'].band_structure.path_connections.count(False)
if np == 1:
    np += 1
fig, axs = plt.subplots(ncols=np,nrows=1,layout="constrained")
bs=result['phonon'].band_structure.plot(axs)
_ = [ ax.set_ylim(ymin,ymax) for ax in axs]
_ = [ ax.get_yaxis().set_visible(False) for i,ax in enumerate(axs) if i > 0  ]
fig.savefig(f"{sysname}-{model}-bands-zoom.svg")
dt = clock.datetime.now() - t
print(f"phonons band structure finished after {dt}")

#result['phonon'].run_mesh(m,with_eigenvectors=True, is_mesh_symmetry=False)
print(f"mesh={m}")
result['phonon'].run_mesh(m)


if thermal:
  t = clock.datetime.now()
  print(f"phonons thermal started at {t}")
  # phonon.run_mesh(mesh=m)
  result['phonon'].run_thermal_properties(t_step=t_step, t_max=t_max, t_min=t_min)
  result["thermal_properties"] = phonon.get_thermal_properties_dict()
  dt = clock.datetime.now() - t
  print(f"phonons thermal finished after {dt}")


  t = clock.datetime.now()
  print(f"phonons save cv started at {t}")
  out=open(f"{sysname}-{model}-cv.dat","w")
  temp = result["thermal_properties"]["temperatures"]
  cv = result["thermal_properties"]["heat_capacity"]
  entropy = result["thermal_properties"]["entropy"]
  free = result["thermal_properties"]["free_energy"]
  result['phonon'].write_yaml_thermal_properties(filename=f"{sysname}-{model}-thermal.yml")

  print("#Temperature [K] | Cv  | H | S ",file=out)
  for x,y,z,tt in zip(temp,cv,free,entropy):
    print(f"{x} {y} {z} {tt}",file=out)
  out.close()
  dt = clock.datetime.now() - t
  print(f"phonons save cv&co finished after {dt}")

if dos:
  t = clock.datetime.now()

  print(f"phonons dos started at {t}")
  # result['phonon'].run_mesh(mesh=m)
  result['phonon'].run_total_dos(freq_pitch=0.01)
  bplt = result['phonon'].plot_total_dos()
  result['phonon'].write_total_dos(f"{sysname}-{model}-dos.dat")

  bplt.savefig(f"{sysname}-{model}-dos.svg")
  dt = clock.datetime.now() - t
  bplt = result['phonon'].plot_band_structure_and_dos()
  bplt.savefig(f"{sysname}-{model}-bands_dos.svg")
  bplt = plot_band_structure_dos_and_pdos(result['phonon'],ymin,ymax)
  plt.savefig(f"{sysname}-{model}-bands_dos-zoom.svg")
  print(f"phonons dos finished after {dt}")

#  t = clock.datetime.now()
#  print(f"phonons pdos started at {t}")
#  result['phonon'].run_projected_dos(freq_pitch=0.03)
#  bplt = result['phonon'].plot_projected_dos()
#  bplt.savefig(f"{sysname}-{model}-pdos.svg")


#  bplt = plot_band_structure_dos_and_pdos(result['phonon'])
#  plt.savefig(f"{sysname}-{model}-bands_pdos.svg")
#  bplt = plot_band_structure_dos_and_pdos(result['phonon'],ymin,ymax)
#  plt.savefig(f"{sysname}-{model}-bands_pdos-zoom.svg")
#  result['phonon'].write_projected_dos(filename=f"{sysname}-{model}-pdos.dat")
#  dt = clock.datetime.now() - t
#  print(f"phonons pdos finished after {dt}")

