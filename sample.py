from ase.io import read,write

from pathlib import Path

import numpy as np

from fpsample import fps_npdu_kdtree_sampling as sample
import argparse

def sampling(data, dims, n_samples):
   n = len(data)
   if n_samples > n:
     n_samples = n-1
   ldata = np.array(data)
   ldata.reshape(n,dims)
   return sample(ldata, n_samples=n_samples, start_idx=0)


def write_samples(frames,inds,f_s):

    for i in inds:
      write(f_s, frames[i], write_info=True, append=True)

parser = argparse.ArgumentParser(
                    prog='split data using descriptors',
                    description='labels data coming from dft and mace descriptors')

parser.add_argument("trajectory")
parser.add_argument("--scale",type=float,default=1.0e5)
parser.add_argument("--n_samples",type=int,default=30)

args = parser.parse_args()

traj = Path(args.trajectory)

sample_file = Path(traj.stem+".sampled.extxyz")
sample_file.unlink(missing_ok=True)

a = read(traj,index=":")
scale=args.scale
ns = args.n_samples

if "system_name" in a[0].info:
  systems = sorted(set([x.info["system_name"] for x in a ]))
else:
  systems = ['all']
np.random.seed(2042)

skip = 0
ind = []
for system in systems:

   print(f"process {system=}")
   if system == 'all':
     lt = a
   else:
     lt = [ x for x in a if x.info["system_name"]==system ]
   n = len(lt)-skip

   specs = set(sorted(lt[0].get_chemical_symbols()))
   De = len(specs)
   desc_per_spec = [ [ x.info[f"mace_mp_{s}_descriptor"]*scale for s in specs ] for x in lt[skip:] ]
   ind_spec = sampling(desc_per_spec,De,ns)
   print(system,"per species",sorted(ind_spec+skip),n)

   Da = len(lt[0])
   desc_per_atom = [ x.arrays[f"mace_mp_descriptors"]*scale for x in lt[skip:] ]
   ind_atom = sampling(desc_per_atom,Da,ns)
   print(system,"per atom",sorted(ind_atom+skip),n)

   Ds = 2
   desc_per_system = [ x.info[f"mace_mp_descriptor"]*scale for x in lt[skip:] ]
   ldps = np.zeros((n,Ds))
   ldps[:,0] = np.array(desc_per_system)
   ind_system = sampling(ldps,Ds,ns)
   print(system,"per system",sorted(ind_system+skip),n)
   lset = set(ind_atom) | set(ind_spec) | set(ind_system)
   ind += [lset]
   print(f"final selection for {system=} ",lset,len(lset))
   write_samples(lt,lset,sample_file)
