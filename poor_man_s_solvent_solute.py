# -*- coding: utf-8 -*-
# Author; alin m elena, alin@elena.re
# Contribs;
# Date: 16-11-2024
# ©alin m elena, GPL v3 https://www.gnu.org/licenses/gpl-3.0.en.html

from ase.io import read,write
from ase.build import molecule as build_molecule
from janus_core.calculations.geom_opt import GeomOpt
from numpy import random,exp,sin,cos,pi,array,sqrt
from janus_core.helpers.mlip_calculators import choose_calculator
from pathlib import Path
import argparse
from ase import Atoms

def random_point_in_sphere(c: (float,float,float), r: float) -> (float,float,float):
    """
    Generates a random point inside a sphere of radius r, centered at c.

    Parameters:
        c (tuple): The center of the sphere as (x, y, z).
        r (float): The radius of the sphere.

    Returns:
        tuple: A point (x, y, z) inside the sphere.
    """
    # Generate a random radius r' from 0 to r
    rad = r * random.rand() ** (1/3)

    # Generate random angles
    theta = random.uniform(0, 2 * pi)  # Azimuthal angle
    phi = random.uniform(0, pi)        # Polar angle

    # Convert spherical to Cartesian coordinates
    x = c[0] + rad * sin(phi) * cos(theta)
    y = c[1] + rad * sin(phi) * sin(theta)
    z = c[2] + rad * cos(phi)

    return (x, y, z)

def random_point_in_ellipsoid(d: (float,float,float), a: float, b: float, c: float) -> (float,float,float):
    """
    Generates a random point inside an ellipsoid with axes a, b, c, centered at d.

    Parameters:
        d (tuple): The center of the ellipsoid as (x, y, z).
        a (float): The semi-axis length of the ellipsoid along the x-axis.
        b (float): The semi-axis length of the ellipsoid along the y-axis.
        c (float): The semi-axis length of the ellipsoid along the z-axis.

    Returns:
        tuple: A point (x, y, z) inside the ellipsoid.
    """
    # Generate random spherical coordinates
    theta = random.uniform(0, 2 * pi)  # Azimuthal angle
    phi = random.uniform(0, pi)        # Polar angle
    rad = random.rand() ** (1/3)      # Radial distance scaled to fit the volume

    # Map spherical coordinates to ellipsoid coordinates
    x = d[0] + a * rad * sin(phi) * cos(theta)
    y = d[1] + b * rad * sin(phi) * sin(theta)
    z = d[2] + c * rad * cos(phi)

    return (x, y, z)

def random_point_in_box(d: (float,float,float), a: float, b: float, c: float) -> (float,float,float):
    """
    Generates a random point inside a box with sides a, b, c, centered at d.

    Parameters:
        d (tuple): The center of the box as (x, y, z).
        a (float): The length of the box along the x-axis.
        b (float): The length of the box along the y-axis.
        c (float): The length of the box along the z-axis.

    Returns:
        tuple: A point (x, y, z) inside the box.
    """
    # Generate random x, y, z coordinates within the box
    x = d[0] + random.uniform(-a * 0.5, a * 0.5)
    y = d[1] + random.uniform(-b * 0.5, b * 0.5)
    z = d[2] + random.uniform(-c * 0.5, c * 0.5)

    return (x, y, z)

def random_point_in_cylinder(c: (float,float,float), r: float, h: float, d: str) -> (float,float,float):
    """
    Generates a random point inside a cylinder with radius r and height h, centered at c.

    Parameters:
        c (tuple): The center of the cylinder as (x, y, z).
        r (float): The radius of the cylinder's base.
        h (float): The height of the cylinder.
        direction (str): direction along which cylinger is oriented

    Returns:
        tuple: A point (x, y, z) inside the cylinder.
    """

    # Generate random polar coordinates for x and y inside the circle
    theta = random.uniform(0, 2 * pi)  # Random angle in radians
    rad = r * sqrt(random.rand())  # Random radius within the circle

    if d == 'z':
      z = c[2] + random.uniform(-h*0.5, h*0.5)
      x = c[0] + rad * cos(theta)
      y = c[1] + rad * sin(theta)
    elif d == 'y':
      y = c[1] + random.uniform(-h*0.5, h*0.5)
      x = c[0] + rad * cos(theta)
      z = c[2] + rad * sin(theta)
    elif d == 'x':
      x = c[0] + random.uniform(-h*0.5, h*0.5)
      y = c[1] + rad * sin(theta)
      z = c[2] + rad * cos(theta)

    return (x, y, z)

def map_interval(x: float, a: float, b: float, c: float, d: float) -> float:
   return c + ((x - a) / (b - a)) * (d - c)

parser = argparse.ArgumentParser(description="insert a number of molecules of a certain type in another system, poor man's packing")

parser.add_argument("--solvent", default="H2O", help="the original box in which you want to add particles, if not provided in empty box will be created, using a, b, c")
parser.add_argument("--solute", default="H2O",help="Name of the molecule to be processed, ase recognisable or ase readable file")
parser.add_argument("--nsolvent", type=int, default=55, help="Target number of molecules to insert")
parser.add_argument("--nsolute", type=int, default=1, help="Target number of molecules to insert")
parser.add_argument("--ntries", type=int, default=50, help="Target number of molecules to insert")
parser.add_argument("--center", type=lambda s: tuple(map(float, s.split(','))),
                        help="center of the insertion zone, in fractional coordinates, e.g., '0.12,0.4,0.5'.")
parser.add_argument("--device", default="cpu",help="where to run")
parser.add_argument("--model", default="small-0b2",help="ml model to use")
parser.add_argument("--arch", default="mace_mp",help="mlip architecture to use")
parser.add_argument("--temperature", type=float, default=300, help="temperature for the montecarlo acceptance rule")
parser.add_argument("--a", type=float, help="side of empty box",default=11.8)
parser.add_argument("--b", type=float, help="side of empty box",default=11.8)
parser.add_argument("--c", type=float, help="side of empty box",default=11.8)

args = parser.parse_args()

solvent = args.solvent
solute = args.solute
nsolvent = args.nsolvent
nsolute = args.nsolute
ntries = args.ntries
center = args.center
a = args.a
b = args.b
c = args.c
device = args.device
model = args.model
arch = args.arch
T = args.temperature

kbT = T*8.6173303e-5

sys = Atoms(cell=[a,b,c],pbc=[True,True,True])
cell = sys.cell.lengths()

print(f"Inserting {nsolute} of {solute} molecules in {nsolvent} of {solvent}.")
print(f"using {arch} model {model} on {device} ")

if center is None:
    center = (cell[0]*0.5,cell[1]*0.5,cell[2]*0.5)
else:
    center *= cell

calc = choose_calculator(arch=arch, model_path=model, device=device, default_dtype="float64")


csys = sys.copy()


def insert_molecule(molecule, x, csys, cell, e, filename, accept=False, m_type="solvent"): 
  itry = 0
  j = x
  while itry < ntries and (not accept):
    try:
      mol = build_molecule(molecule)
      molname = molecule
    except:
      mol = read(molecule)
      molname = Path(molecule).stem
    r = random.rand(3)
    tv = r*cell

    ang = random.rand(3)
    phi = ang[0]*360
    theta = ang[1]*180
    psi = ang[2]*360
    mol.euler_rotate(phi=phi,theta=theta,psi=psi,center=(0.0,0.0,0.0))
    mol.translate(tv)
    tsys = csys.copy() + mol.copy()
    tsys.calc = calc
    en = tsys.get_potential_energy()
    de = en - e
    u = random.rand()
    acc = exp(-de/kbT)
    print(f"{m_type}: old energy={e} new energy={en} change={de} acceptance={acc} random={u}")
    if u <= acc:
      accept = True
    else:
      accept = False
    itry += 1
    if accept:
      x += 1
      csys = tsys.copy()
      print(f"inserted particle {x}")
      write(f"{filename}",csys)
      e = en
  if j == x:
     print(f"failed to insert particle {x} after {ntries} tries")
     geo = GeomOpt(struct_path=f"{filename}",device=device,arch=arch,calc_kwargs={'model_paths':model,'default_dtype':'float64'},filter_kwargs={"hydrostatic_strain": True})
     geo.run()
     e = geo.struct.get_potential_energy()
  return x, e, accept, csys 

p = Path(solute)
q = Path(solvent)

solutename = p.stem if p.is_file() else solute 
solventname = q.stem if q.is_file() else solvent

k = 0 # counts  solvent molecules
i = 0 # counts solute molecules
ksolvent = nsolvent//nsolute # how many solvent molecules to insert per solute
lsol = [i*ksolvent for i in range(nsolute)] + [nsolvent]

e = 0.0
while i < nsolute:
  i, e , accept, csys = insert_molecule(solute,i,csys,cell,e,f"{i+1}{solutename}-{k}{solventname}.cif",accept=False,m_type="solute")
  while k < lsol[i]:
     k, e , accept, csys = insert_molecule(solvent,k,csys,cell,e,f"{i}{solutename}-{k+1}{solventname}.cif",accept=False,m_type="solvent")
     
     
  

geo = GeomOpt(struct_path=f"{i}{solutename}-{k}{solventname}.cif",device=device,arch=arch, calc_kwargs={'model_paths':model,'default_dtype':'float64'},filter_kwargs={"hydrostatic_strain": True})
geo.run()
write (f"{i}{solutename}-{k}{solventname}-opt.cif",geo.struct)
