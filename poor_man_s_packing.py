# -*- coding: utf-8 -*-
# Author; alin m elena, alin@elena.re
# Contribs;
# Date: 16-11-2024
# ©alin m elena, GPL v3 https://www.gnu.org/licenses/gpl-3.0.en.html

from ase.io import read,write
from ase.build import molecule as build_molecule
from janus_core.calculations.geom_opt import GeomOpt
from numpy import random,exp,sin,cos,pi,array,sqrt
from janus_core.helpers.mlip_calculators import choose_calculator
from pathlib import Path
import argparse
from ase import Atoms

def random_point_in_sphere(c: (float,float,float), r: float) -> (float,float,float):
    """
    Generates a random point inside a sphere of radius r, centered at c.

    Parameters:
        c (tuple): The center of the sphere as (x, y, z).
        r (float): The radius of the sphere.

    Returns:
        tuple: A point (x, y, z) inside the sphere.
    """
    # Generate a random radius r' from 0 to r
    rad = r * random.rand() ** (1/3)

    # Generate random angles
    theta = random.uniform(0, 2 * pi)  # Azimuthal angle
    phi = random.uniform(0, pi)        # Polar angle

    # Convert spherical to Cartesian coordinates
    x = c[0] + rad * sin(phi) * cos(theta)
    y = c[1] + rad * sin(phi) * sin(theta)
    z = c[2] + rad * cos(phi)

    return (x, y, z)

def random_point_in_ellipsoid(d: (float,float,float), a: float, b: float, c: float) -> (float,float,float):
    """
    Generates a random point inside an ellipsoid with axes a, b, c, centered at d.

    Parameters:
        d (tuple): The center of the ellipsoid as (x, y, z).
        a (float): The semi-axis length of the ellipsoid along the x-axis.
        b (float): The semi-axis length of the ellipsoid along the y-axis.
        c (float): The semi-axis length of the ellipsoid along the z-axis.

    Returns:
        tuple: A point (x, y, z) inside the ellipsoid.
    """
    # Generate random spherical coordinates
    theta = random.uniform(0, 2 * pi)  # Azimuthal angle
    phi = random.uniform(0, pi)        # Polar angle
    rad = random.rand() ** (1/3)      # Radial distance scaled to fit the volume

    # Map spherical coordinates to ellipsoid coordinates
    x = d[0] + a * rad * sin(phi) * cos(theta)
    y = d[1] + b * rad * sin(phi) * sin(theta)
    z = d[2] + c * rad * cos(phi)

    return (x, y, z)

def random_point_in_box(d: (float,float,float), a: float, b: float, c: float) -> (float,float,float):
    """
    Generates a random point inside a box with sides a, b, c, centered at d.

    Parameters:
        d (tuple): The center of the box as (x, y, z).
        a (float): The length of the box along the x-axis.
        b (float): The length of the box along the y-axis.
        c (float): The length of the box along the z-axis.

    Returns:
        tuple: A point (x, y, z) inside the box.
    """
    # Generate random x, y, z coordinates within the box
    x = d[0] + random.uniform(-a * 0.5, a * 0.5)
    y = d[1] + random.uniform(-b * 0.5, b * 0.5)
    z = d[2] + random.uniform(-c * 0.5, c * 0.5)

    return (x, y, z)

def random_point_in_cylinder(c: (float,float,float), r: float, h: float, d: str) -> (float,float,float):
    """
    Generates a random point inside a cylinder with radius r and height h, centered at c.

    Parameters:
        c (tuple): The center of the cylinder as (x, y, z).
        r (float): The radius of the cylinder's base.
        h (float): The height of the cylinder.
        direction (str): direction along which cylinger is oriented

    Returns:
        tuple: A point (x, y, z) inside the cylinder.
    """

    # Generate random polar coordinates for x and y inside the circle
    theta = random.uniform(0, 2 * pi)  # Random angle in radians
    rad = r * sqrt(random.rand())  # Random radius within the circle

    if d == 'z':
      z = c[2] + random.uniform(-h*0.5, h*0.5)
      x = c[0] + rad * cos(theta)
      y = c[1] + rad * sin(theta)
    elif d == 'y':
      y = c[1] + random.uniform(-h*0.5, h*0.5)
      x = c[0] + rad * cos(theta)
      z = c[2] + rad * sin(theta)
    elif d == 'x':
      x = c[0] + random.uniform(-h*0.5, h*0.5)
      y = c[1] + rad * sin(theta)
      z = c[2] + rad * cos(theta)

    return (x, y, z)

def map_interval(x: float, a: float, b: float, c: float, d: float) -> float:
   return c + ((x - a) / (b - a)) * (d - c)

insertions = ['anywhere','sphere','box','cylinderZ','cylinderY','cylinderX','ellipsoid']
parser = argparse.ArgumentParser(description="insert a number of molecules of a certain type in another system, poor man's packing")

parser.add_argument("--system", help="the original box in which you want to add particles, if not provided in empty box will be created, using ca, cb, cc")
parser.add_argument("--molecule", default="H2O",help="Name of the molecule to be processed, ase recognisable or ase readable file")
parser.add_argument("--nmols", type=int, default=10, help="Target number of molecules to insert")
parser.add_argument("--ntries", type=int, default=50, help="Target number of molecules to insert")
parser.add_argument("--where", default="anywhere", choices=insertions, help=f"how to insert the molecule {insertions}")
parser.add_argument("--center", type=lambda s: tuple(map(float, s.split(','))),
                        help="center of the insertion zone, in fractional coordinates, e.g., '0.12,0.4,0.5'.")
parser.add_argument("--radius", type=float, help="radius of the sphere or cylinder in ˚, depending on the insertion volume")
parser.add_argument("--height", type=float, help="height of cylinder, fractional")
parser.add_argument("--a", type=float, help="side of box, or semi-axis of ellipsoid, fractional, depends on the insertion method")
parser.add_argument("--b", type=float, help="side of box, or semi-axis of ellipsoid, fractional, depends on the insertion method")
parser.add_argument("--c", type=float, help="side of box, or semi-axis of ellipsoid, fractional, depends on the insertion method")
parser.add_argument("--device", default="cpu",help="where to run")
parser.add_argument("--model", default="small-0b2",help="ml model to use")
parser.add_argument("--arch", default="mace_mp",help="mlip architecture to use")
parser.add_argument("--temperature", type=float, default=300, help="temperature for the montecarlo acceptance rule")
parser.add_argument("--ca", type=float, help="side of empty box",default=10.0)
parser.add_argument("--cb", type=float, help="side of empty box",default=10.0)
parser.add_argument("--cc", type=float, help="side of empty box",default=10.0)

args = parser.parse_args()

system = args.system
molecule = args.molecule
nmols = args.nmols
ntries = args.ntries
center = args.center
radius = args.radius
height = args.height
a = args.a
b = args.b
c = args.c
where = args.where
device = args.device
model = args.model
arch = args.arch
T = args.temperature
ca = args.ca
cb = args.cb
cc = args.cc

kbT = T*8.6173303e-5

try:
  sys = read(system)
  sysname = Path(system).stem
except Exception as e:
    sys = Atoms(cell=[ca,cb,cc],pbc=[True,True,True])
    sysname = "empty" 
cell = sys.cell.lengths()

print(f"Inserting {nmols} {molecule} molecules in {sysname}.")
print(f"using {arch} model {model} on {device} ")
print(f"insert in {where} ")

if center is None:
    center = (cell[0]*0.5,cell[1]*0.5,cell[2]*0.5)
else:
    center *= cell
if where == "anywhere":
    a = 1
    b = 1
    c = 1
    print(f"{a=} {b=} {c=}")
elif where == "sphere":
    if radius is None:
        radius = min(cell)*0.5
    print(f"{radius=}")
elif where == "cylinderZ":
    if radius is None:
        radius = min(cell[0],cell[1])*0.5
    if height is None:
        height = 0.5
    print(f"{radius=} {height=}")
elif where == "cylinderY":
    if radius is None:
        radius = min(cell[0],cell[2])*0.5
    if height is None:
        height = 0.5
    print(f"{radius=} {height=}")
elif where == "cylinderX":
    if radius is None:
        radius = min(cell[2],cell[1])*0.5
    if height is None:
        height = 0.5
    print(f"{radius=} {height=}")
elif where == "box":
    if a is None:
        a = 1
    if b is None:
        b = 1
    if c is None:
        c = 1
    print(f"{a=} {b=} {c=}")
elif where == "ellipsoid":
    if a is None:
        a = 0.5
    if b is None:
        b = 0.5
    if c is None:
        c = 0.5
    print(f"{a=} {b=} {c=}")


calc = choose_calculator(arch=arch, model_path=model, device=device, default_dtype="float64")

sys.calc = calc
if len(sys)>0:
  e = sys.get_potential_energy()
else:
  e = 0.0
i = 0

csys = sys.copy()

while i < nmols:
  accept = False
  itry = 0
  j = i
  while itry < ntries and (not accept):
    try:
      mol = build_molecule(molecule)
      molname = molecule
    except:
      mol = read(molecule)
      molname = Path(molecule).stem
    if where == "anywhere":
      r = random.rand(3)
      tv = r*[a,b,c]*cell
    elif where == "sphere":
      tv = random_point_in_sphere(center,radius)
    elif where == "box":
      tv = random_point_in_box(center,cell[0]*a,cell[1]*b,cell[2]*c)
    elif where == 'ellipsoid':
      tv = random_point_in_ellipsoid(center,cell[0]*a,cell[1]*b,cell[2]*c)
    elif where == 'cylinderZ':
      tv = random_point_in_cylinder(center,radius,cell[2]*height,'z')
    elif where == 'cylinderY':
      tv = random_point_in_cylinder(center,radius,cell[1]*height,'y')
    elif where == 'cylinderX':
      tv = random_point_in_cylinder(center,radius,cell[0]*height,'x')
    ang = random.rand(3)
    phi = ang[0]*360
    theta = ang[1]*180
    psi = ang[2]*360
    mol.euler_rotate(phi=phi,theta=theta,psi=psi,center=(0.0,0.0,0.0))
    mol.translate(tv)
    tsys = csys.copy() + mol.copy()
    tsys.calc = calc
    en = tsys.get_potential_energy()
    de = en - e
    u = random.rand()
    acc = exp(-de/kbT)
    print(f"old energy={e} new energy={en} change={de} acceptance={acc} random={u}")
    if u <= acc:
      accept = True
    else:
      accept = False
    itry += 1
    if accept:
      i += 1
      csys = tsys.copy()
      print(f"inserted particle {i}")
      write(f"{sysname}+{i}{molname}.cif",csys)
      e = en
  if j == i:
     print(f"failed to insert particle {i} after {ntries} tries")
     geo = GeomOpt(struct_path=f"{sysname}+{i}{molname}.cif",device=device,calc_kwargs={'model_paths':model,'default_dtype':'float64'},filter_kwargs={"hydrostatic_strain": True})
     geo.run()
     zif = geo.struct.copy()
     e = geo.struct.get_potential_energy()
geo = GeomOpt(struct_path=f"{sysname}+{i}{molname}.cif",device=device,calc_kwargs={'model_paths':model,'default_dtype':'float64'},filter_kwargs={"hydrostatic_strain": True})
geo.run()
write (f"{sysname}+{i}{molname}-opt.cif",geo.struct)
