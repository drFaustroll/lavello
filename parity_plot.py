import argparse
import numpy as np

import matplotlib.pyplot as plt
from sklearn.metrics import r2_score as r2, root_mean_squared_error as rmse

from  ase.io import read,write
from pathlib import Path
from ase.formula import Formula

def add_identity(axes, *line_args, **line_kwargs):
    identity, = axes.plot([], [], *line_args, **line_kwargs)
    def callback(axes):
        low_x, high_x = axes.get_xlim()
        low_y, high_y = axes.get_ylim()
        low = max(low_x, low_y)
        high = min(high_x, high_y)
        identity.set_data([low, high], [low, high])
    callback(axes)
    axes.callbacks.connect('xlim_changed', callback)
    axes.callbacks.connect('ylim_changed', callback)
    return axes

# Parse arguments:
parser = argparse.ArgumentParser(
    description="parity plot from an extxyz"
)
parser.add_argument(
    "--xyz",
    help="input xyz file, output from some calculations",
)
parser.add_argument(
    "--e0s",
    help="input xyz file, output from some calculations to recover e0s used",
)
parser.add_argument(
    "--model",
    default="mace_mp",
    help="model",
)
parser.add_argument(
    "--title",
    help="title for the graph",
)
parser.add_argument("--save", help="File to save the plot", default=None)
args = parser.parse_args()
scale = 1.0e5
nbin = 51

sample_file = args.xyz
model = args.model
title = args.title
sample = read(sample_file,index=":")
e0s_file = args.e0s
p = Path(sample_file)

force_tag_dft = "dft_forces"
force_tag_ml = f"{model}_forces"
e_tag_ml = f"{model}_energy"
e_tag_dft = "dft_energy"
s_tag_ml = f"{model}_stress"
s_tag_dft = "dft_stress"
desc=[]
desc_ps=[]
desc_pa=[]

dft_forces = []
ml_forces = []
dft_e = []
ml_e = []
dft_s = []
ml_s = []
dft_e_pa = []
ml_e_pa = []

e0s={}
if e0s_file:
  for f in read(e0s_file,index=":"):
    if len(f) == 1:
       e0s[f[0].symbol] = (f.info[e_tag_dft],f.info[e_tag_ml])
else:
  e0s = {'Zr': (-2.36081474, -2.36081474), 'Br': (-0.22159175, -0.22159175), 'In': (-0.35470568, -0.35470568), 'I': (-0.18893247, -0.18893247), 'N': (-3.12574641, -3.12574641), 'Ho': (-0.36415458, -0.36415458), 'Ga': (-0.32858019, -0.32858019), 'Hf': (-3.5270668, -3.5270668), 'S': (-0.89160668, -0.89160668), 'Fe': (-3.57490665, -3.57490665), 'O': (-1.90673352, -1.90673352), 'Sr': (-0.06896827, -0.06896827), 'Ce': (-1.09867997, -1.09867997), 'Mg': (-0.09601192, -0.09601192), 'Zn': (-0.16465516, -0.16465516), 'Si': (-0.82583938, -0.82583938), 'Ti': (-2.63984472, -2.63984472), 'Al': (-0.21608163, -0.21608163), 'H': (-1.11761043, -1.11761043), 'P': (-1.88788937, -1.88788937), 'Cd': (-0.16760766, -0.16760766), 'Cl': (-0.25840441, -0.25840441), 'C': (-1.26162075, -1.26162075), 'F': (-0.76714536, -0.76714536), 'Sn': (-0.83592935, -0.83592935), 'Cu': (-0.60324794, -0.60324794)}

print(e0s)

_ = [ Path(f"{p.stem}-{x}.extxyz").unlink(missing_ok=True) for x in ['bad',"pruned"] ]

for i,f in enumerate(sample):
    n = len(f)
    if n == 1:
       write(f"{p.stem}-pruned.extxyz",f,append=True,write_info=True)
       continue
    symbols = f.symbols
    specs = set(symbols)
    ps = [ f'mace_mp_{x}_descriptor' for x in specs]
    ds = [ f.info[d]*scale for d in ps if d in f.info ]
    if len(ds)>0:
        desc_ps.append(ds)
    if f'{model}_descriptor' in f.info:
        desc.append(f.info[f'{model}_descriptor']*scale)
    if f'{model}_descriptors' in f.arrays:
        desc_pa.append(f.arrays[f'{model}_descriptors'].flatten()*scale)

    e = Formula(str(symbols)).count()
    e0_dft = 0.0
    e0_ml = 0.0
    for k in e:
        e0_dft += e[k]*e0s[k][0]
        e0_ml += e[k]*e0s[k][1]
    de = (f.info[e_tag_dft]-e0_dft)/n
    me = (f.info[e_tag_ml]-e0_ml)/n
    f.info["e_diff"] = me-de
    if abs(me-de) > 5.0e-2:
        print(f"{i} {len(f)} {me:.4e} {de:.4e} {me-de:.4e}")
        write(f"{p.stem}-bad.extxyz",f,append=True,write_info=True)
    else:
        write(f"{p.stem}-pruned.extxyz",f,append=True,write_info=True)

    dft_e_pa.append(de)
    ml_e_pa.append(me)
    dft_e.append(f.info[e_tag_dft])
    ml_e.append(f.info[e_tag_ml])
    dft_forces.append(f.arrays[force_tag_dft].flatten())
    ml_forces.append(f.arrays[force_tag_ml].flatten())
    ml_s.append(f.info[s_tag_ml])
    s  = f.info[s_tag_dft]
    
    if len(s) == 3:
        x = [s[0][0],s[1][1],s[2][2],s[1][2],s[0][2],s[0][1]]
        f.info[s_tag_dft] = x
        dft_s.append(x)
    elif len(s) == 9:
        # 0 1 2
        # 3 4 5
        # 6 7 8
        x = [s[0],s[4],s[8],s[5],s[2],s[1]]
        f.info[s_tag_dft] = x
        dft_s.append(x)
    else:
        dft_s.append(s)

dft_forces = np.concatenate(dft_forces, axis=0)
ml_forces = np.concatenate(ml_forces, axis=0)

dft_s = np.concatenate(dft_s, axis=0)
ml_s = np.concatenate(ml_s, axis=0)

if desc_pa:
  desc_pa = np.concatenate(desc_pa, axis=0)
if desc_ps:
  desc_ps = np.concatenate(desc_ps, axis=0)

ea_r = r2(dft_e_pa,ml_e_pa)
e_r = r2(dft_e,ml_e)
f_r = r2(dft_forces,ml_forces)
s_r = r2(dft_s,ml_s)

ea_rmse = rmse(dft_e_pa,ml_e_pa)
e_rmse = rmse(dft_e,ml_e)
s_rmse = rmse(dft_s,ml_s)
f_rmse = rmse(dft_forces,ml_forces)

nr = 2
fs = 8
if desc or desc_pa or desc_ps:
    nr = 3
    fs = 12
fig, axs = plt.subplots(nrows=nr, ncols=4, figsize=(16, fs))

axs[0,0].set_title("Energy/atom")
axs[0,0].set_xlabel("DFT energy/atom [eV]")
axs[0,0].set_ylabel("ML energy/atom [eV]")
axs[0,0].scatter(dft_e_pa, ml_e_pa)
axs[0,0].annotate(f"r²={ea_r:.4f}",xy=(0.1,0.95),xycoords='axes fraction')
axs[0,0].annotate(f"rmse={ea_rmse*1000:.3f} meV",xy=(0.1,0.9),xycoords='axes fraction')
axs[0,0].set_aspect('equal')
add_identity(axs[0,0], color='orange', ls='--')

axs[0,1].set_title("Potential energy")
axs[0,1].set_xlabel("DFT energy [eV]")
axs[0,1].set_ylabel("ML energy [eV]")
axs[0,1].scatter(dft_e, ml_e)
axs[0,1].annotate(f"r²={e_r:.4f}",xy=(0.1,0.95),xycoords='axes fraction')
axs[0,1].annotate(f"rmse={e_rmse*1000:.3f} meV",xy=(0.1,0.9),xycoords='axes fraction')
axs[0,1].set_aspect('equal')
add_identity(axs[0,1], color='orange', ls='--')

axs[0,2].set_title("Force components")
axs[0,2].set_xlabel("DFT forces [eV/Å]")
axs[0,2].set_ylabel("ML forces [eV/Å]")
axs[0,2].scatter(dft_forces, ml_forces)
axs[0,2].annotate(f"r²={f_r:.4f}",xy=(0.1,0.95),xycoords='axes fraction')
axs[0,2].annotate(f"rmse={f_rmse*1000:.3f} meV/Å³",xy=(0.1,0.9),xycoords='axes fraction')
axs[0,2].set_aspect('equal')
add_identity(axs[0,2], color='orange', ls='--')

axs[0,3].set_title("Stress (Voigt) components")
axs[0,3].set_xlabel("DFT stress [eV/Å³]")
axs[0,3].set_ylabel("ML stress [eV/Å³]")
axs[0,3].scatter(dft_s, ml_s)
axs[0,3].annotate(f"r²={s_r:.4f}",xy=(0.1,0.95),xycoords='axes fraction')
axs[0,3].annotate(f"rmse={s_rmse*1000:.3f} meV/Å³",xy=(0.1,0.9),xycoords='axes fraction')
axs[0,3].set_aspect('equal')
add_identity(axs[0,3], color='orange', ls='--')

if title:
  plt.suptitle(f"{title}")
else:
  plt.suptitle(f"Parity plots and histograms DFT/ML data - {p.stem}")

axs[1,0].set_title("Energy/atom distribution")
axs[1,0].hist(ml_e_pa,label="ML",bins=nbin)
axs[1,0].hist(dft_e_pa,label="DFT",bins=nbin, alpha=0.75)
axs[1,0].set_xlabel("energy/atom [eV/Å]")
axs[1,0].set_ylabel("count")
axs[1,0].legend()

axs[1,1].set_title("Potential energy distribution")
axs[1,1].hist(ml_e, label="ML",bins=nbin)
axs[1,1].hist(dft_e, label='DFT',alpha=0.75,bins=nbin)
axs[1,1].set_xlabel("energy [eV]")
axs[1,1].set_ylabel("count")
axs[1,1].legend()

axs[1,2].set_title("Force components distribution")
axs[1,2].hist(ml_forces,label="ML",bins=nbin)
axs[1,2].hist(dft_forces,label="DFT",bins=nbin,alpha=0.75)
axs[1,2].set_xlabel("force [eV/Å]")
axs[1,2].set_ylabel("count")
axs[1,2].legend()

axs[1,3].set_title("Stress (Voigt) components distribution")
axs[1,3].hist(ml_s,label="ML",bins=nbin)
axs[1,3].hist(dft_s,label="DFT",bins=nbin,alpha=0.75)
axs[1,3].set_xlabel("stress [eV/Å³]")
axs[1,3].set_ylabel("count")
axs[1,3].legend()

if desc:
    axs[2,0].set_title("Descriptors per system")
    axs[2,0].hist(desc,bins=nbin)
if len(desc_ps)>0:
    axs[2,1].set_title("Descriptors per species")
    axs[2,1].hist(desc_ps,bins=nbin)
if len(desc_pa)>0:
    axs[2,2].set_title("Descriptors per atom")
    axs[2,2].hist(desc_pa,bins=nbin)

plt.tight_layout()
if args.save is None:
    plt.show()
else:
    plt.savefig(args.save)
