# -*- coding: utf-8 -*-
# Author; alin m elena, alin@elena.re
# Contribs;
# Date: 13-05-2024
# ©alin m elena, GPL v3 https://www.gnu.org/licenses/gpl-3.0.en.html
from mantid.simpleapi import Abins
import matplotlib.pyplot as plt
import mantid
import argparse

cli=argparse.ArgumentParser()
cli.add_argument(
  "--phonopy",
  type=str,
  default='',
  help = 'provide the phonopy yaml file. default: %(default)s'
  )

cli.add_argument(
  "--out_mantid",
  type=str,
  default='abins-output',
  help = 'provide the mantind output space. default: %(default)s'
  )
cli.add_argument(
  "--temperature",
  type=float,
  default=10.0,
  help = 'provide temperature in K, default %(default)s'
  )

cli.add_argument(
  "--spectrum",
  type=str,
  default='spectrum.dat',
  help = 'file to write spectrum. default: %(default)s'
  )


args = cli.parse_args()
phonopy = args.phonopy
aout = args.out_mantid
temperature = args.temperature
spectrum = args.spectrum

a = mantid.simpleapi.Abins(
        VibrationalOrPhononFile=f"{phonopy}",
        AbInitioProgram="FORCECONSTANTS",
        OutputWorkspace=f"{aout}",
        TemperatureInKelvin=f"{temperature}",
        SumContributions=True,
        ScaleByCrossSection="Total",
        QuantumOrderEventsNumber="2",
        Autoconvolution=True,
        Instrument="TOSCA",
        Setting="Backward (TOSCA)",
    )


workspace = mantid.simpleapi.mtd["abins-output_total"]

frequency_bins, intensities = workspace.extractX(), workspace.extractY()

# Convert 2-D arrays with one row to 1-D arrays for plotting
intensities = intensities.flatten()
frequency_bins = frequency_bins.flatten()

# Convert (N+1) bin edges to N midpoints to plot against N intensities
frequency_midpoints = (frequency_bins[:-1] + frequency_bins[1:]) / 2

fig, ax = plt.subplots()
ax.plot(frequency_midpoints, intensities, label="TOSCA backscattering sim.")
ax.set_xlabel(r"Frequency / cm$^{-1}$")
ax.set_ylabel("Intensity")
with open(f"{spectrum}","w") as fd:
    for x,y in zip(frequency_midpoints,intensities):
        print(f"{x} {y}",file=fd)
plt.show()
