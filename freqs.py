# -*- coding: utf-8 -*-
# Author; alin m elena, alin@elena.re
# Contribs;
# Date: 20-09-2024
# ©alin m elena, GPL v3 https://www.gnu.org/licenses/gpl-3.0.en.html
import yaml
import sys
import lzma

freq = sys.argv[1]
with lzma.open(freq, 'rb') as file:
    print(f"decompress file={freq}")
    dc = file.read()
    print("load yaml")
    data = yaml.safe_load(dc)
for phonon in data['phonon']:
    q = phonon['q-position']
    band = phonon['band']
    for f in band:
        if f['frequency'] < 0.0:
            print(q,f['frequency'])

